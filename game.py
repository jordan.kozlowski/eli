from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor
from panda3d.core import (AmbientLight, DirectionalLight, Vec4, WindowProperties, CollisionTraverser,
						CollisionHandlerPusher, CollisionTube)
from gameobject import *


class Game(ShowBase):


	def __init__(self):
		ShowBase.__init__(self)
		self.disableMouse()

		# SETTING THE WINDOW #
		properties = WindowProperties()
		properties.setSize(1000, 750)
		self.win.requestProperties(properties)
		# ------------------ #

		# LOADING MODELS #
		self.environment = self.loader.loadModel('./assets/environment')
		self.environment.reparentTo(self.render)
		self.tempActor = Actor('./assets/act_p3d_chan', {'walk': './assets/a_p3d_chan_run'})
		self.tempActor.setPos(0, 7, 0)
		self.tempActor.getChild(0).setH(180)
		# -------------- #

		# SETTING COLLISION DETECTION #
		self.cTrav = CollisionTraverser()
		self.pusher = CollisionHandlerPusher()		# PREVENTS NOMINATED SOLID OBJECTS FROM COLLIDING WITH OTHER OBJECTS
		colliderNode = CollisionNode('player')
		colliderNode.addSolid(CollisionSphere(0, 0, 0, .3))
		collider = self.tempActor.attachNewNode(colliderNode)
		#collider.show() 	# DISPLAYS THE COLLIDER
		self.pusher.addCollider(collider, self.tempActor)	# ACTOR WILL BE MOVED AS A RESULT OF COLLISION
		self.pusher.setHorizontal(True)
		self.cTrav.addCollider(collider, self.pusher)
		# --------------------------- #

		# SETTING THE LIGHTS #
		ambientLight = AmbientLight('ambient light')
		ambientLight.setColor(Vec4(.2, .2, .2, 1))
		self.ambientLightNodePath = self.render.attachNewNode(ambientLight)
		self.render.setLight(self.ambientLightNodePath) # THIS TELLS P3D TO SET THE LIGHT ON THE WHOLE SCENE
		mainLight = DirectionalLight('main light')
		self.mainLightNodePath = self.render.attachNewNode(mainLight)
		self.mainLightNodePath.setHpr(45, -45, 0)   # TURN IT AROUND BY 45 DEGREES AND TILT IT DOWN BY 45 DEGREES
		self.render.setLight(self.mainLightNodePath)
		# ------ #

		# CAMERA #
		self.cam.setPos(0, 0, 32) 					# MOVES CAMERA HIGH ABOVE THE SCREEN. OFFSETS IT ALONG THE Z-AXIS
		self.cam.setP(-90) 							# TILTS THE CAMERA DOWN BY SETTING ITS PITCH
		# -----  #

		self.render.setShaderAuto()
		self.define_keystrokes()
		self.set_up_keymap()
		self.set_up_collision_tubes()				# PREVENTS PLAYER FROM WALKING THROUGH THE WALLS
		self.updateTask = taskMgr.add(self.update, 'update')
		self.player = Player()
		self.tempEnemy = WalkingEnemy(Vec3(5, 0, 0))
		self.tempTrap = TrapEnemy(Vec3(-2, 7, 0))
		self.pusher.add_in_pattern('%fn-into-%in')
		self.accept('trapEnemy-into-wall', self.stopTrap)
		self.accept('trapEnemy-into-trapEnemy', self.stopTrap)
		self.accept('trapEnemy-into-player', self.trapHitsSomething)
		self.accept('trapEnemy-into-walkingEnemy', self.trapHitsSomething)


	def stopTrap(self, entry):
		collider = entry.getFromNodePath()
		if collider.hasPythonTag('owner'):
			trap = collider.getPythonTag('owner')
			trap.moveDirection = 0
			trap.ignorePlayer = False


	def trapHitsSomething(self, entry):
		collider = entry.getFromNodePath()
		if collider.hasPythonTag('owner'):
			trap = collider.getPythonTag('owner')

			if trap.moveDirection == 0:
				return

			collider = entry.getIntoNodePath()
			if collider.hasPythonTag('owner'):
				obj = collider.getPythonTag('owner')
				if isinstance(obj, Player):
					if not trap.ignorePlayer:
						obj.alterHealth(-1)
						trap.ignorePlayer = True
				else:
					obj.alterHealth(-10)


	def set_up_keymap(self):
		self.keymap = {
		'up': False,
		'down': False,
		'left': False,
		'right': False,
		'shoot': False
		}


	def define_keystrokes(self):
		self.accept('w', self.updateKeyMap, ['up', True])
		self.accept('w-up', self.updateKeyMap, ['up', False])
		self.accept('s', self.updateKeyMap, ['down', True])
		self.accept('s-up', self.updateKeyMap, ['down', False])
		self.accept('a', self.updateKeyMap, ['left', True])
		self.accept('a-up', self.updateKeyMap, ['left', False])
		self.accept('d', self.updateKeyMap, ['right', True])
		self.accept('d-up', self.updateKeyMap, ['right', False])
		self.accept('f', self.updateKeyMap, ['shoot', True])
		self.accept('f-up', self.updateKeyMap, ['shoot', False])


	def set_up_collision_tubes(self):
		# SETTING COLLISION TUBES FOR ALL 4 WALLS
		wallSolid = CollisionTube(-8.0, 0, 0, 8.0, 0, 0, .2)
		wallNode = CollisionNode('wall')
		wallNode.addSolid(wallSolid)
		wall = self.render.attachNewNode(wallNode)
		wall.setY(8.0)
		#wall.show() 	# DISPLAYS COLLISION TUBE

		wallSolid = CollisionTube(-8.0, 0, 0, 8.0, 0, 0, .2)
		wallNode = CollisionNode('wall')
		wallNode.addSolid(wallSolid)
		wall = self.render.attachNewNode(wallNode)
		wall.setY(-8.0)
		#wall.show()

		wallSolid = CollisionTube(0, -8.0, 0, 0, 8.0, 0, .2)
		wallNode = CollisionNode('wall')
		wallNode.addSolid(wallSolid)
		wall = self.render.attachNewNode(wallNode)
		wall.setX(8.0)
		#wall.show()

		wallSolid = CollisionTube(0, -8.0, 0, 0, 8.0, 0, .2)
		wallNode = CollisionNode('wall')
		wallNode.addSolid(wallSolid)
		wall = self.render.attachNewNode(wallNode)
		wall.setX(-8.0)
		#wall.show()


	def updateKeyMap(self, control_name, control_state):
		self.keymap[control_name] = control_state


	def update(self, task):
		dt = globalClock.getDt()
		self.player.update(self.keymap, dt)
		self.tempEnemy.update(self.player, dt)
		self.tempTrap.update(self.player, dt)
		if self.keymap['up']:
			self.tempActor.setPos(self.tempActor.getPos() + Vec3(0, 5.0 * dt, 0))
		if self.keymap['down']:
			self.tempActor.setPos(self.tempActor.getPos() + Vec3(0, -5.0 * dt, 0))
		if self.keymap['left']:
			self.tempActor.setPos(self.tempActor.getPos() + Vec3(-5.0 * dt, 0, 0))
		if self.keymap['right']:
			self.tempActor.setPos(self.tempActor.getPos() + Vec3(5.0 * dt, 0, 0))
		return task.cont


game = Game()
game.run()
